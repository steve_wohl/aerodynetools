#' @title Read a Aerdyne .str file
#' @description Reads a aerodyne str file into a dataframe. The Timestamp variable is renamed to 'DateTime', and converted to POSIXct. 
#' @param filename The file to be read
#' @param nlabelline How many lines at the top of the file contain labels? Will be automated in a future version.
#' @param dateTimeMethod Either 'lubridate' (the default) or 'POSIXct'. Extra arguments to either can be provided via \dots .
#' @param posixctFormat If dateTimeMethod is 'POSIXct', the format string for the datetime.
#' @param addFileName If TRUE (the default), adds a new variable 'Source', the filename from which the data was read.
#' @param rmDupDateTime Logical, whether to remove duplicated datetimes from the data (default is FALSE).
#' @param \dots Extra arguments to the datetime conversion method.
#' @export
#' @return A dataframe
readStr <- function(filename, nlabelline=1, 
                     dateTimeMethod=c("POSIXct"),
                     addFileName=TRUE, rmDupDateTime=FALSE, ...){
  
  getStr <- function(fn){
    h <- readLines(fn, n=nlabelline)
    h <- regmatches(h,regexpr("(?<=SPEC:)(.+)",h,perl=TRUE))
    h <- c("DateTime",strsplit(h, "," )[[1]])
    dat <- read.csv(fn, skip=nlabelline, header=FALSE, na.strings="NAN", sep = " ")
    names(dat) <- make.names(h)
    dat[,ncol(dat)] <-NULL
    dat<-dat[complete.cases(dat),]
    return(dat)
  }
  dat <- try(getStr(filename))
  if(inherits(dat,"try-error")){
    message("Could not read file ", filename, ". It is probably not TOA5 or otherwise corrupted.\n Returning NULL.")
    return(NULL)
  }
  
  if(dateTimeMethod == "POSIXct")
    dat$DateTime <- as.POSIXct(dat$DateTime, tz = "UTC", origin = "1904-01-01")
  
  dat$Date <- as.Date(dat$DateTime)
  
  if(rmDupDateTime)dat <- dat[!duplicated(dat$DateTime),]
  
  if(addFileName)dat$Source <- basename(filename)
  
  return(dat)
}
